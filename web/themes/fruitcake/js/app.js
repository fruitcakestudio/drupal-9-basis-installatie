jQuery( document ).ready(function($) {
	$('.js-searchToggle').click(function(evt){
		evt.preventDefault();
		
		$('body').toggleClass('body--searchActive');
	})
	
	$('.photos--carousel').owlCarousel({
	    loop:true,
	    margin:30,
	    nav:false,
	    dots:true,
	    navText:["<i class='fal fa-chevron-left'></i>","<i class='fal fa-chevron-right'></i>"],
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3,
	            nav: true
	        }
	    }
	})
	

});