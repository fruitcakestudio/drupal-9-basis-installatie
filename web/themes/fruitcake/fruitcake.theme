<?php

/**
 * @file
 * Functions to support theming in the SASS Starterkit subtheme.
 */

use Drupal\Core\Form\FormStateInterface;
/**
 * Implements hook_form_system_theme_settings_alter() for settings form.
 *
 * Replace Barrio setting options with subtheme ones.
 */
function fruitcake_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  $form['components']['navbar']['fruitcake_navbar_top_background']['#options'] = array(
      'bg-primary' => t('Primary'),
      'bg-secondary' => t('Secondary'),
      'bg-light' => t('Light'),
      'bg-dark' => t('Dark'),
      'bg-white' => t('White'),
      'bg-transparent' => t('Transparent'),
  );
  $form['components']['navbar']['fruitcake_navbar_background']['#options'] = array(
      'bg-primary' => t('Primary'),
      'bg-secondary' => t('Secondary'),
      'bg-light' => t('Light'),
      'bg-dark' => t('Dark'),
      'bg-white' => t('White'),
      'bg-transparent' => t('Transparent'),
  );
}

function fruitcake_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  // Add content type suggestions.
  if ($node = \Drupal::request()->attributes->get('node')) {
    array_splice($suggestions, 1, 0, 'page__node__' . $node->getType());
  }
}

function fruitcake_preprocess_node(&$variables) {
  // Allowed view modes
  $view_mode = $variables['view_mode']; // Retrieve view mode
  $allowed_view_modes = ['full']; // Array of allowed view modes (for performance so as to not execute on unneeded nodes)
 
  // If view mode is in allowed view modes list, pass to THEME_add_regions_to_node()
  if(in_array($view_mode, $allowed_view_modes)) {
    // Allowed regions (for performance so as to not execute for unneeded region)
    $allowed_regions = ['landing_news'];
    fruitcake_add_regions_to_node($allowed_regions, $variables);
  }
  
  // Load the current user.
	// $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

}

function fruitcake_preprocess_page(&$variables) {

}

/**
* THEME_add_regions_to_node
*/
 
function fruitcake_add_regions_to_node($allowed_regions, &$variables) {
  // Retrieve active theme
  $theme = \Drupal::theme()->getActiveTheme()->getName();
 
  // Retrieve theme regions
  $available_regions = system_region_list($theme, 'REGIONS_ALL');
 
  // Validate allowed regions with available regions
  $regions = array_intersect(array_keys($available_regions), $allowed_regions);
 
  // For each region
  foreach ($regions as $key => $region) {
 
    // Load region blocks
	$blocks = \Drupal::entityTypeManager()->getStorage('block')->loadByProperties(array('theme' => $theme, 'region' => $region));
	
    // Sort ‘em
    uasort($blocks, 'Drupal\block\Entity\Block::sort');
 
    // Capture viewable blocks and their settings to $build
    $build = array();
    foreach ($blocks as $key => $block) {
      if ($block->access('view')) {
	      $builder = \Drupal::entityTypeManager()->getViewBuilder('block');
		$build[$key] = $builder->view($block, 'block');
      }
    }
 
    // Add build to region
    $variables[$region] = $build;
  }
}

function fruitcake_preprocess_links__language_block(&$variables) {

  foreach ($variables['links'] as $i => $link) {
    // @var \Drupal\language\Entity\ConfigurableLanguage $linkLanguage
    $linkLanguage = $link['link']['#options']['language'];
    $variables['links'][$i]['link']['#title'] = $linkLanguage->get('id');
  }
}